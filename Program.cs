﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaardiPakk
{
    class Program
    {
        static void Main(string[] args)
        {//esimene variant
            List<int> pakk = Enumerable.Range(1, 52).ToList(); //nii saan poest segamata kaardipaki
            Random r = new Random(); //kui segada vaja, siis randomit vaja 

            //esimene variant ühest pakist teise
            List<int> segatudpakk = new List<int>(); //tühi pakk
            while(pakk.Count > 0)
            {
                int mitmes = r.Next(pakk.Count); //leiame juhusliku kaardi (number 0...kaartide arv)
                segatudpakk.Add(pakk[mitmes]); // paneme ta teise pakki
                pakk.RemoveAt(mitmes); //kustutame ta esimesest ära
            }

            for (int i = 0; i < 52; i++)
                    {
                    Console.Write($"{segatudpakk[i]:00} {(i % 13 == 12 ? "\n" : "\t")}");  
                    }
            //teine variant
            segatudpakk = new List<int>(); //pakk tühjakss
            while(segatudpakk.Count < 52)
                 {
                int uus = r.Next(52);
                if (!segatudpakk.Contains(uus)) segatudpakk.Add(uus);
                 }
            Console.WriteLine("\nsegatud pakk\n");
            for (int i = 0; i < 52; i++)

            {
                Console.Write($"{segatudpakk[i]:00} {(i % 13 == 12 ? "\n" : "\t")}");
            }
        }
    }
}
